import React, { useState, useEffect } from 'react';

const IdolList = () => {
    const [idols, setIdols] = useState([])
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem('favorites')) || [])

    // fetch : 전화기
    useEffect(() => {
        fetch('http://localhost:3000/profile.json')
            .then(response => response.json())
            .then(data => setIdols(data))
    }, [])

    // 직렬화, 스토리지에 저장
    useEffect(() => {
        localStorage.setItem('favorites', JSON.stringify(favorites))
    }, [favorites])

    // info를 ...favorites에 저장해라
    const addFavorite = (info) => {
        if(favorites.find(i => i.id === info.id)) {
            setFavorites(favorites.filter(i => i.id !== info.id))
        } else {
            setFavorites([...favorites, info])
        }
    }

    return (
        <div className="hsb-idol-lists">
            {idols.map(info => (
                <div key={info.id}>
                    <div className="hsb-idol-set">
                        <div className="hsb-idol-img"></div>
                        <p className="hsb-idol-name">{info.groupName} : {info.memberName}</p>
                        <button onClick={() => addFavorite(info)} className="hsb-favorite-btn">
                            {favorites.find(i => i.id === info.id) ? '찜 취소' : '찜 하기♥'}
                        </button>
                    </div>
                </div>
            ))}
        </div>
    )
}

export default IdolList;