import './App.css';
import IdolList from "./pages/IdolList";
import React from "react";
import { Routes, Route } from "react-router-dom";
import DefaultLayout from "./layouts/defaultLayout";

function App() {
  return (
    <div className="App">
        <div className="hsb-idol-list">
            <div className="hsb-top">
                <div className="hsb-title-section">
                    <h1 className="hsb-title">💜 최애 리스트 💜</h1>
                    <p className="hsb-sub-title">당신의 최애를 찜해보세요 👈</p>
                </div>
                <button className="hsb-move-btn">찜 목록 가기</button>
            </div>
            <Routes>
                <Route path={"/"} element={ <DefaultLayout><IdolList/></DefaultLayout> } />
            </Routes>

        </div>
    </div>
  );
}

export default App;
