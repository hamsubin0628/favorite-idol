import React from 'react';

const defaultLayout = ({ children }) => {
    return (
        // 프레그먼트
        <>
            <div>헤더</div>
            <main>{children}</main>
            <footer>footer</footer>
        </>
    )
}

export default defaultLayout;